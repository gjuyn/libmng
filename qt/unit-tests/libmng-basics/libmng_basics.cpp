//**************************************************************************************************
//   libmng_basics.cpp
//**************************************************************************************************

#include <QtTest>

#include "libmng.h"

//**************************************************************************************************

extern "C" {
static mng_ptr MyAlloc(mng_size_t iLen)
{
    return calloc(1, iLen);
}
}

//**************************************************************************************************

extern "C" {
static void MyFree(mng_ptr iPtr, mng_size_t iLen)
{
    Q_UNUSED(iLen);
    free(iPtr);
}
}

//**************************************************************************************************

class libmng_basics : public QObject
{
    Q_OBJECT

public:
    libmng_basics();
    ~libmng_basics();

private slots:
    void test_version();

    void test_functions_data();
    void test_functions();

    void test_initcleanup();

};

//**************************************************************************************************

libmng_basics::libmng_basics()
{
    // no-op
}

//**************************************************************************************************

libmng_basics::~libmng_basics()
{
    // no-op
}

//**************************************************************************************************

void libmng_basics::test_version()
{
    QByteArray shVrs1(MNG_VERSION_TEXT);
    QByteArray shVrs2(mng_version_text());
    QCOMPARE(shVrs1, shVrs2);

    QCOMPARE(mng_version_so(),      static_cast<mng_uint8>(MNG_VERSION_SO));
    QCOMPARE(mng_version_dll(),     static_cast<mng_uint8>(MNG_VERSION_DLL));
    QCOMPARE(mng_version_major(),   static_cast<mng_uint8>(MNG_VERSION_MAJOR));
    QCOMPARE(mng_version_minor(),   static_cast<mng_uint8>(MNG_VERSION_MINOR));
    QCOMPARE(mng_version_release(), static_cast<mng_uint8>(MNG_VERSION_RELEASE));
    QCOMPARE(mng_version_beta(),    static_cast<mng_bool>(MNG_VERSION_BETA));
}

//**************************************************************************************************

void libmng_basics::test_functions_data()
{
    QTest::addColumn<QString>("shFunction");
    QTest::addColumn<int>("ihMajor");
    QTest::addColumn<int>("ihMinor");
    QTest::addColumn<int>("ihRelease");
    QTest::addColumn<bool>("bhResult");

    QTest::newRow("mng_hello")              << "mng_hello"              << 0 << 0 << 0 << false;
    QTest::newRow("mng_cleanup")            << "mng_cleanup"            << 1 << 0 << 0 << true;
    QTest::newRow("mng_copy_chunk")         << "mng_copy_chunk"         << 1 << 0 << 5 << true;
    QTest::newRow("mng_get_currframdelay")  << "mng_get_currframdelay"  << 1 << 0 << 9 << true;
    QTest::newRow("mng_get_lastbackchunk")  << "mng_get_lastbackchunk"  << 1 << 0 << 3 << true;
    QTest::newRow("mng_get_totalframes")    << "mng_get_totalframes"    << 1 << 0 << 5 << true;
    QTest::newRow("mng_get_viewgammaint")   << "mng_get_viewgammaint"   << 1 << 0 << 0 << true;
    QTest::newRow("mng_getcb_openstream")   << "mng_getcb_openstream"   << 1 << 0 << 0 << true;
    QTest::newRow("mng_getcb_processterm")  << "mng_getcb_processterm"  << 1 << 0 << 2 << true;
    QTest::newRow("mng_getchunk_basi")      << "mng_getchunk_basi"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_chrm")      << "mng_getchunk_chrm"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_evnt")      << "mng_getchunk_evnt"      << 1 << 0 << 5 << true;
    QTest::newRow("mng_getchunk_fpri")      << "mng_getchunk_fpri"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_hist")      << "mng_getchunk_hist"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_iccp")      << "mng_getchunk_iccp"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_ijng")      << "mng_getchunk_ijng"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_jdat")      << "mng_getchunk_jdat"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_magn")      << "mng_getchunk_magn"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_ordr")      << "mng_getchunk_ordr"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_past")      << "mng_getchunk_past"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_text")      << "mng_getchunk_text"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_time")      << "mng_getchunk_time"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_trns")      << "mng_getchunk_trns"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getchunk_ztxt")      << "mng_getchunk_ztxt"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_getlasterror")       << "mng_getlasterror"       << 1 << 0 << 0 << true;
    QTest::newRow("mng_initialize")         << "mng_initialize"         << 1 << 0 << 0 << true;
    QTest::newRow("mng_iterate_chunks")     << "mng_iterate_chunks"     << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_basi")      << "mng_putchunk_basi"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_chrm")      << "mng_putchunk_chrm"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_evnt")      << "mng_putchunk_evnt"      << 1 << 0 << 5 << true;
    QTest::newRow("mng_putchunk_fpri")      << "mng_putchunk_fpri"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_hist")      << "mng_putchunk_hist"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_iccp")      << "mng_putchunk_iccp"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_ijng")      << "mng_putchunk_ijng"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_jdat")      << "mng_putchunk_jdat"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_magn")      << "mng_putchunk_magn"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_ordr")      << "mng_putchunk_ordr"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_past")      << "mng_putchunk_past"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_text")      << "mng_putchunk_text"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_time")      << "mng_putchunk_time"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_trns")      << "mng_putchunk_trns"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_putchunk_ztxt")      << "mng_putchunk_ztxt"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_read")               << "mng_read"               << 1 << 0 << 0 << true;
    QTest::newRow("mng_read_resume")        << "mng_read_resume"        << 1 << 0 << 0 << true;
    QTest::newRow("mng_read_pushdata")      << "mng_read_pushdata"      << 1 << 0 << 8 << true;
    QTest::newRow("mng_reset")              << "mng_reset"              << 1 << 0 << 0 << true;
    QTest::newRow("mng_set_bgcolor")        << "mng_set_bgcolor"        << 1 << 0 << 0 << true;
    QTest::newRow("mng_set_cacheplayback")  << "mng_set_cacheplayback"  << 1 << 0 << 2 << true;
    QTest::newRow("mng_set_displaygamma")   << "mng_set_displaygamma"   << 1 << 0 << 0 << true;
    QTest::newRow("mng_set_doprogressive")  << "mng_set_doprogressive"  << 1 << 0 << 2 << true;
    QTest::newRow("mng_set_maxcanvassize")  << "mng_set_maxcanvassize"  << 1 << 0 << 0 << true;
    QTest::newRow("mng_set_outputsrgb")     << "mng_set_outputsrgb"     << 1 << 0 << 1 << true;
    QTest::newRow("mng_set_speed")          << "mng_set_speed"          << 1 << 0 << 0 << true;
    QTest::newRow("mng_set_srgbimplicit")   << "mng_set_srgbimplicit"   << 1 << 0 << 1 << true;
    QTest::newRow("mng_set_usebkgd")        << "mng_set_usebkgd"        << 1 << 0 << 0 << true;
    QTest::newRow("mng_set_viewgamma")      << "mng_set_viewgamma"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_setcb_errorproc")    << "mng_setcb_errorproc"    << 1 << 0 << 0 << true;
    QTest::newRow("mng_setcb_openstream")   << "mng_setcb_openstream"   << 1 << 0 << 0 << true;
    QTest::newRow("mng_setcb_processgamma") << "mng_setcb_processgamma" << 1 << 0 << 0 << true;
    QTest::newRow("mng_setcb_processmend")  << "mng_setcb_processmend"  << 1 << 0 << 1 << true;
    QTest::newRow("mng_setcb_processterm")  << "mng_setcb_processterm"  << 1 << 0 << 2 << true;
    QTest::newRow("mng_setcb_readdata")     << "mng_setcb_readdata"     << 1 << 0 << 0 << true;
    QTest::newRow("mng_setcb_refresh")      << "mng_setcb_refresh"      << 1 << 0 << 0 << true;
    QTest::newRow("mng_setcb_releasedata")  << "mng_setcb_releasedata"  << 1 << 0 << 8 << true;
    QTest::newRow("mng_setcb_writedata")    << "mng_setcb_writedata"    << 1 << 0 << 0 << true;
    QTest::newRow("mng_status_creating")    << "mng_status_creating"    << 1 << 0 << 0 << true;
    QTest::newRow("mng_status_displaying")  << "mng_status_displaying"  << 1 << 0 << 0 << true;
    QTest::newRow("mng_status_dynamic")     << "mng_status_dynamic"     << 1 << 0 << 5 << true;
    QTest::newRow("mng_status_running")     << "mng_status_running"     << 1 << 0 << 0 << true;
    QTest::newRow("mng_supports_func")      << "mng_supports_func"      << 1 << 0 << 5 << true;
    QTest::newRow("mng_trapevent")          << "mng_trapevent"          << 1 << 0 << 5 << true;
    QTest::newRow("mng_updatemngheader")    << "mng_updatemngheader"    << 1 << 0 << 0 << true;
    QTest::newRow("mng_write")              << "mng_write"              << 1 << 0 << 0 << true;
}

//**************************************************************************************************

void libmng_basics::test_functions()
{
    QFETCH(QString, shFunction);
    QFETCH(int, ihMajor);
    QFETCH(int, ihMinor);
    QFETCH(int, ihRelease);
    QFETCH(bool,  bhResult);

    mng_uint8 ihMajor2   = 0;
    mng_uint8 ihMinor2   = 0;
    mng_uint8 ihRelease2 = 0;
    mng_bool  bhResult2  = MNG_FALSE;

    bhResult2 = mng_supports_func(shFunction.toLatin1().data(), &ihMajor2, &ihMinor2, &ihRelease2);
    QCOMPARE(ihMajor,   static_cast<int>(ihMajor2));
    QCOMPARE(ihMinor,   static_cast<int>(ihMinor2));
    QCOMPARE(ihRelease, static_cast<int>(ihRelease2));
    QCOMPARE(bhResult,  static_cast<bool>(bhResult2));
}

//**************************************************************************************************

void libmng_basics::test_initcleanup()
{
    mng_retcode ihError    = 0;
    mng_handle  phHandle   = nullptr;
    mng_ptr     phUserData = static_cast<mng_ptr>(this);

    phHandle = mng_initialize(phUserData, MyAlloc, MyFree, nullptr);
    QVERIFY(phHandle != nullptr);

    ihError = mng_reset(phHandle);
    QCOMPARE(ihError, 0);

    ihError = mng_cleanup(&phHandle);
    QCOMPARE(ihError, 0);
    QCOMPARE(phHandle, nullptr);
}

//**************************************************************************************************

QTEST_APPLESS_MAIN(libmng_basics)

#include "libmng_basics.moc"

//**************************************************************************************************
