QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += \
    MNG_FULL_CMS \
    MNG_SUPPORT_READ \
    MNG_SUPPORT_WRITE \
    MNG_SUPPORT_DISPLAY \
    MNG_ERROR_TELLTALE \
    MNG_SUPPORT_TRACE \
    MNG_TRACE_TELLTALE \
    MNG_USE_LIBLCMS2 \
#    MNG_OPTIMIZE_CHUNKREADER \
#    MNG_OPTIMIZE_CHUNKINITFREE \
#    MNG_OPTIMIZE_CHUNKASSIGN \

LIBS += -lz -ljpeg -llcms2

INCLUDEPATH += ../../../source
VPATH += ../../../source

HEADERS += \
    libmng.h \
    libmng_conf.h \
    libmng_types.h \
    libmng_error.h \
    libmng_trace.h \
    libmng_memory.h \
    libmng_data.h \
    libmng_chunks.h \
    libmng_chunk_descr.h \
    libmng_chunk_io.h \
    libmng_chunk_prc.h \
    libmng_objects.h \
    libmng_object_prc.h \
    libmng_filter.h \
    libmng_pixels.h \
    libmng_dither.h \
    libmng_read.h \
    libmng_write.h \
    libmng_display.h \
    libmng_cms.h \
    libmng_jpeg.h \
    libmng_zlib.h

SOURCES += \
    libmng_error.c \
    libmng_trace.c \
    libmng_hlapi.c \
    libmng_prop_xs.c \
    libmng_callback_xs.c \
    libmng_chunk_descr.c \
    libmng_chunk_io.c \
    libmng_chunk_prc.c \
    libmng_chunk_xs.c \
    libmng_object_prc.c \
    libmng_filter.c \
    libmng_pixels.c \
    libmng_dither.c \
    libmng_read.c \
    libmng_write.c \
    libmng_cms.c \
    libmng_display.c \
    libmng_jpeg.c \
    libmng_zlib.c \
    libmng_basics.cpp
