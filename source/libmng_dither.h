/* ************************************************************************** */
/* *             For conditions of distribution and use,                    * */
/* *                see copyright notice in libmng.h                        * */
/* ************************************************************************** */
/* *                                                                        * */
/* * project   : libmng                                                     * */
/* * file      : libmng_dither.h                copyright (c) 2018 G.Juyn   * */
/* * version   : 2.1.0                                                      * */
/* *                                                                        * */
/* * purpose   : Dithering routines (definition)                            * */
/* *                                                                        * */
/* * author    : G.Juyn                                                     * */
/* *                                                                        * */
/* * comment   : Definition of the dithering routines                       * */
/* *                                                                        * */
/* * changes   : 0.5.1 - 05/08/2000 - G.Juyn                                * */
/* *             - changed strict-ANSI stuff                                * */
/* *                                                                        * */
/* *             0.9.2 - 08/05/2000 - G.Juyn                                * */
/* *             - changed file-prefixes                                    * */
/* *                                                                        * */
/* *             1.0.5 - 08/19/2002 - G.Juyn                                * */
/* *             - B597134 - libmng pollutes the linker namespace           * */
/* *                                                                        * */
/* *             2.1.0 - 29/11/2018 - G.Juyn                                * */
/* *             - moved repository to GitLab                               * */
/* *             - code cleanup                                             * */
/* *                                                                        * */
/* ************************************************************************** */

#if defined(__BORLANDC__) && defined(MNG_STRICT_ANSI)
#pragma option -A                      /* force ANSI-C */
#endif

#ifndef _libmng_dither_h_
#define _libmng_dither_h_

#include "libmng.h"
#include "libmng_data.h"

/* ************************************************************************** */

mng_retcode mng_dither_a_row (mng_datap  pData,
                              mng_uint8p pRow);

/* ************************************************************************** */

#endif /* _libmng_dither_h_ */

/* ************************************************************************** */
/* * end of file                                                            * */
/* ************************************************************************** */
